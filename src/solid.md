---
marp: true
theme: lection
# theme: gaia
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

# Принципы SOLID


#

#

#

#

#

#

#

##### Константин Володин

---

<!-- _paginate: false -->

# Принципы SOLID

__SOLID__ — это аббревиатура, обозначающая первые пять принципов объектно-ориентированного программирования, сформулированные __Робертом С. Мартином__ (также известным как __дядя Боб__).

Эти принципы устанавливают __практики__, помогающие создавать программное обеспечение, которое можно __обслуживать__ и __расширять__ по мере развития проекта. 

Применение этих практик поможет избавиться от __плохого кода__, __оптимизировать код__.

![bg right:30%](img/robert.jpg)

---
<!-- _class: oneimage -->

# Книги

![h:485px](img/books.png)

---

# Принципы SOLID

1) __SRP - Single Responsibility Principle__ 
Принцип единственной ответственности

2) __OCP - Open/Closed__ 
Принцип открытости/закрытости

3) __LSP - Liskov Substitution__
Принцип подстановки (Барбары) Лисков

4) __ISP - Interface Segregation__ 
Принцип разделения интерфейса

5) __DIP - Dependency Inversion__ 
Принцип инверсии зависимостей

---

# Немного терминологии

## 1. Класс

A class — in the context of Java — is a template used to create objects and to define object data types and methods. Classes are categories, and objects are items within each category. All class objects should have the basic class properties.

## 2. Абстрактный класс

An abstract class is nothing but a class that is declared using the abstract keyword. It also allows us to declare method signatures using the abstract keyword (abstract method) and forces its subclasses to implement all the declared methods.

---

# Немного терминологии

## 3. Интерфейс

An __interface__ is a __description of the actions__ that an __object__ can do.


An interface is a __fully abstract class__. 
It includes a group of abstract methods (methods without a body). 
We use the __interface keyword__ to create an interface in Java. 
For example, 
```java
interface Language { 
    public void getType(); 
    public void getVersion();
}
interface Drawable { 
    public void draw(); 
}
```

---
# Немного терминологии

## 4. Concrete class

A concrete class is a class that we can create __an instance of__, using the __new keyword__. 

In other words, it's a full implementation of its blueprint. A concrete class is complete.


## 5. Cohesion

Cohesion in Java is the Object-Oriented principle most closely associated with making sure that a class is designed with a single, well-focused purpose. In object-oriented design, cohesion refers all to how a single class is designed. 

---
<!-- _class: diagram -->

![](img/cohesion.png)

---

__Cohesion__ is an indication of how related and focused the responsibilities of an software element are.

__Coupling__ refers to how strongly a software element is connected to other elements.

The software element could be __class, package, component, subsystem or a system__. And while designing the systems it is recommended to have software elements that have __High cohesion__ and support __Low coupling__.

__Low cohesion__ results in monolithic classes that are difficult to maintain, understand and reduces re-usablity. 

Similarly __High Coupling__ results in classes that are tightly coupled and changes tend not be non-local, difficult to change and reduces the reuse.

---

# 1. SRP - Принцип единственной ответственности
Single Responsibility Principle

<br>
<br>

## У класса должна быть одна и только одна причина для изменения, то есть у класса должна быть только одна работа.

<br>
<br>

Получаем __God Оbject__ (богоподобный объект) - если все в одном классе.

__God Оbject__ - антипаттерн.

---
<!-- _class: codeinvert -->
```python
class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total = 0
        for i in range(len(self.prices)):
            total += self.quantities[i] * self.prices[i]
        return total

    def pay(self, payment_type, security_code):
        if payment_type == "debit":
            print("Processing debit payment type")
            print(f"Verifying security code: {security_code}")
            self.status = "paid"
        elif payment_type == "credit":
            print("Processing credit payment type")
            print(f"Verifying security code: {security_code}")
            self.status = "paid"
        else:
            raise Exception(f"Unknown payment type: {payment_type}")
```

---
<!-- _class: codeinvert -->

```python
order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
order.pay("debit", "0372846")
```

---
<!-- _class: diagram -->

# Диаграмма классов

![h:370px](diagrams/SRP.png)

---
<!-- _class: codeinvert -->
```python
class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total = 0
        for i in range(len(self.prices)):
            total += self.quantities[i] * self.prices[i]
        return total

class PaymentProcessor:
    def pay_debit(self, order, security_code):
        print("Processing debit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

    def pay_credit(self, order, security_code):
        print("Processing credit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"
```

---
<!-- _class: codeinvert -->
```python
order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
processor = PaymentProcessor()
processor.pay_debit(order, "0372846")
```

---
<!-- _class: diagram -->

# Диаграмма классов SRP

![h:580px](diagrams/SRP_fix.png)

---

# 2. OCP - Принцип открытости/закрытости
Open/Closed Principle

<br>

## Объекты или сущности должны быть открыты для расширения, но закрыты для изменения.

## Это означает, что у нас должна быть возможность расширять класс без изменения самого класса.

---
<!-- _class: codeinvert -->
```python
class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total = 0
        for i in range(len(self.prices)):
            total += self.quantities[i] * self.prices[i]
        return total

class PaymentProcessor:
    def pay_debit(self, order, security_code):
        print("Processing debit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

    def pay_credit(self, order, security_code):
        print("Processing credit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"
```

---
<!-- _class: codeinvert -->
```python
from abc import ABC, abstractmethod

class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, order, security_code):
        pass

class DebitPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing debit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

class CreditPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing credit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
processor = DebitPaymentProcessor()
processor.pay(order, "0372846")
```

---
<!-- _class: diagram -->

# Диаграмма классов OCP
 
![h:570px](diagrams/OCP.png)

---
<!-- _class: codeinvert -->
```python
class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, order, security_code):
        pass

class DebitPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing debit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

class CreditPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing credit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

class PaypalPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing paypal payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"
```

---
<!-- _class: diagram -->

# Диаграмма классов OCP расширенная

![h:570px](diagrams/OCP_extended.png)

---
# 3. LSP - Принцип подстановки (Барбары) Лисков
Liskov Substitution

<br>

## Пусть q(x) будет доказанным свойством объектов x типа T. Тогда q(y) будет доказанным свойством объектов y типа S, где S является подтипом T.

Это означает, что каждый подкласс или производный класс должен быть заменяемым на базовый класс или родительский класс.

---
<!-- _class: codeinvert -->
```python
class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, order, security_code):
        pass

class CreditPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing credit payment type")
        print(f"Verifying security code: {security_code}")
        order.status = "paid"

class PaypalPaymentProcessor(PaymentProcessor):
    def pay(self, order, security_code):
        print("Processing paypal payment type")
        print(f"Using email address: {security_code}")
        order.status = "paid"
```

---
<!-- _class: codeinvert -->
```python
class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, order):
        pass

class CreditPaymentProcessor(PaymentProcessor):
    def __init__(self, security_code):
        self.security_code = security_code
    def pay(self, order):
        print("Processing credit payment type")
        print(f"Verifying security code: {self.security_code}")
        order.status = "paid"

class PaypalPaymentProcessor(PaymentProcessor):
    def __init__(self, email_address):
        self.email_address = email_address
    def pay(self, order):
        print("Processing paypal payment type")
        print(f"Using email address: {self.email_address}")
        order.status = "paid"

order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
processor = PaypalPaymentProcessor("user@user.com")
processor.pay(order)
```

---
<!-- _class: diagram -->

# Диаграмма классов LSP

![h:600px](diagrams/LSP.png)

---

# 4. ISP - Принцип разделения интерфейса
Interface Segregation 

<br>

## Клиент никогда не должен быть вынужден реализовывать интерфейс, который он не использует, или клиенты не должны вынужденно зависеть от методов, которые они не используют.

---
<!-- _class: codeinvert -->
```python
class PaymentProcessor(ABC):
    @abstractmethod
    def auth_sms(self, code):
        pass
    @abstractmethod
    def pay(self, order):
        pass

class DebitPaymentProcessor(PaymentProcessor):

    def __init__(self, security_code):
        self.security_code = security_code
        self.verified = False

    def auth_sms(self, code):
        print(f"Verifying SMS code {code}")
        self.verified = True
    
    def pay(self, order):
        if not self.verified:
            raise Exception("Not authorized")
        print("Processing debit payment type")
        print(f"Verifying security code: {self.security_code}")
        order.status = "paid"
```
---
<!-- _class: codeinvert -->
```python
class CreditPaymentProcessor(PaymentProcessor):

    def __init__(self, security_code):
        self.security_code = security_code

    def auth_sms(self, code):
        raise Exception("Credit card payments don't support SMS code authorization.")

    def pay(self, order):
        print("Processing credit payment type")
        print(f"Verifying security code: {self.security_code}")
        order.status = "paid"

order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
processor = DebitPaymentProcessor("2349875")
processor.auth_sms(465839)
processor.pay(order)
```

---
<!-- _class: diagram -->

# Диаграмма классов ISP

![w:1230px](diagrams/ISP.png)

---
<!-- _class: diagram -->

# Диаграмма классов ISP расширенная

![h:600px](diagrams/ISP_extended.png)

---
<!-- _class: diagram -->

# Диаграмма классов ISP true separated interfaces

![w:1230px](diagrams/ISP_extended_3.png)

---
<!-- _class: diagram -->

# Диаграмма классов ISP c композицией

![w:1200px](diagrams/ISP_extended_2.png)

---
<!-- _class: codeinvert -->
```python
order = Order()
order.add_item("Keyboard", 1, 50)
order.add_item("SSD", 1, 150)
order.add_item("USB cable", 2, 5)

print(order.total_price())
authorizer = SMSAuthorizer()
# authorizer.verify_code(465839)
processor = PaypalPaymentProcessor("hi@arjancodes.com", authorizer)
processor.pay(order)
```

---

# 5. DIP - Принцип инверсии зависимостей
Dependency Inversion 

<br>

## Сущности должны зависеть от абстракций, а не от чего-то конкретного. Это означает, что модуль высокого уровня не должен зависеть от модуля низкого уровня, но они оба должны зависеть от абстракций.

Этот принцип открывает возможности декаплинга (decoupling) или понижения связности.

---
<!-- _class: diagram -->

# Диаграмма классов DIP

![w:1230px](diagrams/DIP.png)

---
<!-- _class: diagram -->

# Диаграмма классов DIP 2

![w:1230px](diagrams/DIP2.png)

---

# Принципы vs Паттерны

__Design Principles:__

1. Design principles are core abstract principles that we are supposed to follow while designing software. 
2. Remember they aren't concrete - rather abstract. 
3. They can be applied in any language, on any platform regardless of the state as long as we are within the permissible conditions.

__Examples:__

1. Encapsulate what varies.
2. Program to interfaces, not to implementations.
3. Depend upon abstractions. Do not depend upon concrete classes.

---

# Принципы vs Паттерны

__Design Patterns:__

1. They are solutions to real-world problems that pop up time and again, so instead of reinventing the wheel, we follow the design patterns that are well-proven, tested by others, and safe to follow. 
2. Now, design patterns are specific; there are terms and conditions only in which a design pattern can be applied.

__Examples:__

1. Singleton Pattern (One class can only have one instance at a time)
2. Adapter Pattern (Match interface of different classes)

---

# Задание лр / на неделю / ДЗ

1. Прочитать доступные источники по теме занятия

2. Для каждого из принципов изучить код лаборторных работ на предмет наличия нарушения его.

3. Для каждого из принципов предложить улучшение кода лабораторных работ.

---

# Источники

1. https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design-ru

2. https://www.youtube.com/watch?v=pTB30aXS77U

3. https://github.com/ArjanCodes/betterpython/tree/main/9%20-%20solid

4. https://stackoverflow.com/questions/31317141/whats-the-difference-between-design-patterns-and-design-principles#:~:text=A%20principle%20is%20an%20abstraction,that%20solves%20a%20particular%20problem.

5. ...