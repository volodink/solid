.PHONY: all clean live 

all: clean images
	marp --html src/solid.md -o build/solid-definitions.pdf

images:
	java -jar .devcontainer/lib/plantuml.jar src/diagrams/*.puml -tpng
	
live: images
	marp --html --server ./src/

clean:
	rm -rf build
	rm -rf builds
	rm -rf src/diagrams/*.png
